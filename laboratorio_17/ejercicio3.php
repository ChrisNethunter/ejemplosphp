<!DOCTYPE html>
<html>
	<head>
		<title>Ejercicio 3</title>
	</head>
	<body>
		<form method="POST" action="prom3.php">
			<label>Nombre del Pasciente</label>
			<input type="text" name="nombre">
			<br>

			<label>Peso en Kilogramos</label>
			<input type="number" name="kilogramos">
			<br>

			<label>Estatura en metros</label>
			<input type="number" name="estatura">
			<br>

			<input type="submit">
		</form>

		<?php

			include "conect.php";

			if ($mysqli == false ) {
				echo "Ha habido un error <br>";

			}else{

				echo "Esta conectada la base de datos <br>";

				$sql = "CREATE TABLE pasciente (
												id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
												nombre VARCHAR(30) NOT NULL,
												kilogramos int(10) NOT NULL,
												estatura int(50) NOT NULL
											 )";


				if ($mysqli->query($sql) === TRUE) {
				    echo "Table pasciente created successfully <br>";
				} else {
				    echo "Error creating table: " .$mysqli->error;
				}

				$mysqli->close();	
			}
		?>
	</body>
</html>