<!DOCTYPE html>
<html>
	<head>
		<title>Ejercicio 2</title>
	</head>
	<body>
		<form method="POST" action="prom2.php">
			<label>Nombre del Vendedor</label>
			<input type="text" name="nombre">
			<br>

			<label>Cantidad Automoviles Vendidos</label>
			<input type="number" name="cantidad">
			<br>

			<label>Precio Total Automoviles Vendidos</label>
			<input type="number" name="precio">
			<br>

			<input type="submit">
		</form>


		<?php
			include "conect.php";

			if ($mysqli == false ) {
				echo "Ha habido un error <br>";

			}else{

				echo "Esta conectada la base de datos <br>";

				$sql = "CREATE TABLE Ventas (
												id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
												nombre VARCHAR(30) NOT NULL,
												cantidad_vehiculos int(10) NOT NULL,
												precio int(50)
											 )";

				if ($mysqli->query($sql) === TRUE) {
				    echo "Table Ventas created successfully <br>";
				} else {
				    echo "Error creating table: " .$mysqli->error;
				}

				$mysqli->close();	
			}
		?>
	</body>
</html>