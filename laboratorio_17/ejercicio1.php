<!DOCTYPE html>
<html>
	<head>
		<title>Ejercicio 1 Php</title>
	</head>
	<body>

		
		<form method="POST" action="promedio.php">

			<label>Parcial 1</label>
			<input type="number" name="parcial1">
			<br>

			<label>Parcial 2</label>
			<input type="number" name="parcial2">
			<br>

			<label>Parcial 3</label>
			<input type="number"  name="parcial3">
			<br>

			<label>Examen Final</label>
			<input type="number" name="examenfinal">
			<br>

			<label>Trabajo Final</label>
			<input type="text" name="trabajofinal">

			<input type="submit">

		</form>

		<?php 

			include "conect.php";

			if ($mysqli == false ) {
				echo "Ha habido un error <br>";

			}else{

				echo "Esta conectada la base de datos <br>";

				$sql = "CREATE TABLE notas (
												id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
												parcial1 INT(30) NOT NULL,
												parcial2 int(10) NOT NULL,
												parcial3 int(50) NOT NULL,
												trabajo_final int(50) NOT NULL,
												examen_final int(50) NOT NULL
											 )";

				if ($mysqli->query($sql) == TRUE) {
				    echo "Table Notas created successfully <br>";
				} else {
				    echo "Error creating table: " .$mysqli->error;
				}

				$mysqli->close();	
			}

		?>
	</body>
</html>
