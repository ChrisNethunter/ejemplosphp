<?php
session_start();
extract($_REQUEST); //recoger todas las variables que pasan Método GET o POST
// con ese $_REQUEST ya podemos acceder a todos los datos que mandamos en el form
// de acuerdo al name que le pusimos a cada input el form
// si definimos mal el name se totea esos

require "../Modelo/conexionBasesDatos.php";
require "../Modelo/Paciente.php";
// despues metemos el modelo paciente porque es el que se va encargar de recibir los datos y actualizarlo
//Creamos el objeto Paciente
//aqui lo llamamos nuevamente con new() es decir que ya podriamos acceder a todos los metodos o funciones
//en este caso estamos creando un paciente. pero no en base de datos sino como constructor de la clase
// para poder acceder a los datos mas facil dentro de la clase
// claros hasta ahi ? si atento aca
// listo ya despues de creado podemos llamar actualizarPaciente()
$objPaciente= new Paciente();
$objPaciente->CrearPaciente($_REQUEST['identificacion'],$_REQUEST['nombres'],$_REQUEST['apellidos'],
$_REQUEST['fechaNacimiento'],$_REQUEST['sexo']);
// porque hicimos esto asi $objPaciente->CrearPaciente() por que asi nos queda mas facil llamar
// la funcion o metodo  $objPaciente->actualizarPaciente(); vea que no se le esta enviando ningun parametro
// esta vacia. porque como los enviamos en el formulario cierto ? y lo quse hacemos es llamar simplemente la funciones
// porque si la hubieramos hecho de otra manera es asi.

// y aqui no hubieramos llamado crear $objPaciente->CrearPaciente si no $objPaciente->actualizarPaciente($_REQUEST['identificacion'],$_REQUEST['nombres'],$_REQUEST['apellidos'],
//$_REQUEST['fechaNacimiento'],$_REQUEST['sexo']);
// me hago entender ? si claro eso si no lo sabia por eso cojemos crear paciente como constructor y lo podemos coger cuantas veces queramos exacto
// sin llenarnos de parametros en la funciones que hagamos en la clase o modelo


//NOTA = CLASE Y MODELO = SON LO MISMO no sabia esto tampoco
// METODO Y FUNCION = SON LO MISMO

// pero todo esta mas claro ? huy si bastante
// listo como ya podemos llamar actualizar paciente ahora toca mandar un estado de la ejecucion a traves de la url

$resultado = $objPaciente->actualizarPaciente();
// es decir aqui estan llamando la funcion y esto esta retornando algo cierto ?si
//volvamos al ejemplo del HRTime\PerformanceCounter

// aqui es donde lo vamos a llamar
$resultado = $objPaciente->hola2("Juan Pablo");
//si es verdadero lo que validamos en el modelo o clase la variable resultado va ser igual a
// $resultado = "hola Juan Pablo";
// sino es igual la variable sera $resultado = false;

//cuando hacemos una condicion asi
/*if($mivariablecualquiera){
	es porque es true o verdadera
	o si existe
}else{
	aqui es falsa o no existe
}*/

// es porque vamos a devolver variabloes llenas o true o false es decir
// que la funcion puede devolver true tambien
// dudas ? no

/*
mire que la condicion dice que si $resultado esta todo bien manda una variable msj= 1 que significa exito
sino manda msj=2 que significa error
bien ? si esas variables estan definidas en otro lado con en mensaje
exacto estan en el mismo formulario

y note que tambien necesita mostrar la variable pac para que cuando llamemos actualizar asi este bien o mal
necesitamos mostrar donde estamos parados

porque si no mandamos nuevamente la variable &pac=".$_REQUEST['identificacion'] en la url (en esto es lo que estaba mas perdido me preguntaba donde esta es variable declarada o algo y eso fue lo que me estallo)
lo que va a pasar es que el mensaje que va aparecer de exito o de error en un formulario en blanco
no vamos saber a que paciente fue alq que se le ejecuto la accion me hago entender ? si
esa variable se declaro desde la tabla

pero en este caso volvemos a declarar la variable pac pero con lo que enviamos del formulario
si queda claro ?si cris
*/
// en teoria ya deberia funcionar
//probemos no actualiza
// no ?

//el error debe estar es en los campos de la funcion
if ($resultado)
	header ("location:http://localhost/laboratorio_18/CentroMedico/Vista/index2.php?pag=actualizarPaciente2&msj=1&pac=".$_REQUEST['identificacion']);
else
	header ("location:http://localhost/laboratorio_18/CentroMedico/Vista/index2.php?pag=actualizarPaciente2&msj=2&pac=".$_REQUEST['identificacion']);

?>
