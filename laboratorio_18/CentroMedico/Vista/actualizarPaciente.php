<?php
  require "../Modelo/conexionBasesDatos.php";
  require "../Modelo/Paciente.php";
  $objPaciente=new Paciente();
  $paciente=$objPaciente->consultarPacientes();
  ?>
  <h1 align="center">LISTADO DE PACIENTES</h1>
  <table width="70%" border="1" align="center">
    <tr align="center" bgcolor="#cc0000" class="texto">
      <td>Identificacion</td>
      <td>Nombres</td>
      <td>Apellidos</td>
      <td>Fecha Nacimiento</td>
      <td>Sexo</td>
      <td>Modificar</td>
    </tr>
   <?php
   /*con el while recorremos todos los datos
   pero podemos hacer algo cada vez que recorra los datos
   en este caso el while dice que por cada paciente va mostrar una fila es decir un <tr>
   y despues dentro de esa fila columnas <td>.
   como dije anteriormente nosotros creamos una posicion nueva que se llama modificar
   esa hace referencia hace pero no se le esta pasando el identificador

   porque el modelo de paciente hay una funcion consultar paciente y para que solo le traiga uno necesita
   ese identificador como parametro

   acuerdese que para mandar una variable en la url es despues del signo ?
   en este caso index2.php? y ahi mandamos pac pero le tenemos que concatenar la identificacion
   index2.php?pac='. $registro->medIdentificacion .'
   todo esta dentro del while
   vamos a verificar que cuandole le demos click en la url la variable pac me mande el identificador

   Algina duda ? ya ahi me quedo Claro
   le voy a dejar esos comentarios para que tenga como base para que explique
   hagale y la actualizacion esta bien?


   */
  while($registro=$paciente->fetch_object())
  {
  ?>
    <tr>
      <td><?php echo $registro->pacIdentificacion?></td>
      <td><?php echo $registro->pacNombres?></td>
      <td><?php echo $registro->pacApellidos?></td>
      <td><?php echo $registro->pacFechaNacimiento?></td>
      <td><?php echo $registro->pacSexo?></td>
      <td><?php echo '<a href="index2.php?pac='. $registro->pacIdentificacion .'&pag=actualizarPaciente2">Modificar</a> '?></td>
    </tr>
   <?php
 }  //cerrando el ciclo while lo que no sabia es que esa variable adquiria el valor asi
 // porque estamos accediendo a todos los datos y los recorremos uno a uno y lo asignamos con la etiqueta a
 // mas claro ? huy si jeje

?>
</table>
