<?php
  require "../Modelo/conexionBasesDatos.php";
  require "../Modelo/Paciente.php";
  //Creamos el objeto Paciente
  /*
    Aqui lo que hacemos es llamar el modelo Paciente
    solo se esta llamando con el new Paciente();
    pero no se esta creando todavia

    en este caso lo unico que hace es llamar al paciente
    para despues llamar el metodo consultar paciente
    y le mandamos la variable que nos llega de la url

    es decir que podriamos usar otras funciones que tenga la clase
    siempre y cuando la llamemos
    new Modelo(); este es un ejemplo como lo llamemos

    es decir otro ejemplo

    esa funcion la llame hola y lo que recibe es un parametro de un nombre
    para que el salude como ya llamamos la clase con el new podemos llamar el
    metodo siempre y cuando le mandemos el parametro

  */
  $objPaciente= new Paciente();
  $paciente = $objPaciente->consultarPaciente($pac);

  // aqui llamamos una variable y lo que hace es usar $objPaciente porque es la que contiene el
  // el llamado a la clase y simplemente accedo a la funcion y le mando el parametro
  // en este caso la mande una cadena "juan pablo" pero podria hacerlo mejor
  // nombre una variable con el valor y la mando como parametro
  // es decir que si me llegara de una url usted ya sabe como hacerlo o de un formulario
  // dudas ? no ya mas claro :)

  $nombreSaludar = "Juan Pablo";
  $hola = $objPaciente -> hola($nombreSaludar);
  echo $hola;

  while($registro=$paciente->fetch_object())
  {

    // ahora sigue el form para actualizar
    // lo que hacemos en el action es enviarlo al Controlador
    // si se acuerda el modelo nos crea la estructura de datos
    // y el modelo es el que se encarga de acceder a esos datos y mostrarlos en la vista
    // ese validarActualizarPaciente se va encargar de recibir los datos y procesarlos
    // vamos a verlo

    echo '
    <form id="form1" name="form1" method="post" action="../Controlador/validarActualizarPaciente.php">
      <table width="42%" border="0" align="center">
        <tr bgcolor="#cc0000" class="texto">
          <td colspan="2" align="center">ACTUALIZAR PACIENTE</td>
        </tr>
        <tr>
          <td width="28%" align="right" bgcolor="#fbec88">Identificación</td>
          <td width="72%"><label for="identificacion"></label>
          <input name="identificacion" type="text" id="identificacion" size="40"value="'.$registro->pacIdentificacion. '"  required /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#fbec88">Nombres</td>
          <td><input name="nombres" type="text" id="nombres" size="40" value="'.$registro->pacNombres. '" required /></td>
        </tr>
        <tr>
          <td height="25" align="right" bgcolor="#fbec88">Apellidos</td>
          <td><input name="apellidos" type="text" id="apellidos" size="40" value="'.$registro->pacApellidos. '" required/></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#fbec88">Fecha Nacimiento</td>
        </tr>
        <td><input name="fechaNacimiento" type="date" id="fechaNacimiento" style="width:270px" value="'.$registro->pacFechaNacimiento. '" required/></td>
        <tr>
        <td height="25" align="right" bgcolor="#fbec88">Sexo</td>
        <td><input name="sexo" type="text" id="sexo" size="40" value="'.$registro->pacSexo. '" required/></td>
        </tr>
        <tr bgcolor="#cc0000" class="texto">
          <td colspan="2" align="center" bgcolor="#cc0000"><input type="submit" name="button" id="button" value="Enviar" /></td>
        </tr>
      </table>
    </form>';
  }

  if ($msj==1)
  echo '<p align="center" >Se ha Editado el Paciente Correctamente';
  if ($msj==2)
  echo '<p align="center"> Problemas al Editar el Paciente, favor Revisar';

?>
