<?php
  require "../Modelo/conexionBasesDatos.php";
  require "../Modelo/Medico.php";
  //Creamos el objeto Paciente
  $objMedico= new Medico();
  $medico = $objMedico->consultarMedico($med);

  while($registro=$medico->fetch_object())
  {
    echo '
    <form id="form1" name="form1" method="post" action="../Controlador/validarActualizarMedico.php">
      <table width="42%" border="0" align="center">
        <tr bgcolor="#cc0000" class="texto">
          <td colspan="2" align="center">ACTUALIZAR MEDICO</td>
        </tr>
        <tr>
          <td width="28%" align="right" bgcolor="#fbec88">Identificación</td>
          <td width="72%"><label for="identificacion"></label>
          <input name="identificacion" type="text" id="identificacion" size="40"  value="'.$registro->medIdentificacion. '" required /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#fbec88">Nombres</td>
          <td><input name="nombres" type="text" id="nombres" size="40" value="'.$registro->medNombres. '" required /></td>
        </tr>
        <tr>
          <td height="25" align="right" bgcolor="#fbec88">Apellidos</td>
          <td><input name="apellidos" type="text" id="apellidos" size="40" value="'.$registro->medApellidos. '" required/></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#fbec88">Especialidad</td>
          <td><input name="especialidad" type="text" id="especialidad" style="width:270px" value="'.$registro->medEspecialidad. '" required/></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#fbec88">Telefono</td>
          <td><input name="telefono" type="text" id="telefono" style="width:270px" value="'.$registro->medTelefono. '" required/></td>
        </tr>

        <tr>
          <td align="right" bgcolor="#fbec88">Email</td>
          <td><input name="email" type="text" id="email" style="width:270px" value="'.$registro->medCorreo. '" required/></td>
        </tr>
        <tr bgcolor="#cc0000" class="texto">
          <td colspan="2" align="center" bgcolor="#cc0000"><input type="submit" name="button" id="button" value="Enviar" /></td>
        </tr>
      </table>
    </form>';
  }

  if ($msj==1)
  echo '<p align="center" >Se ha Editado el Medico Correctamente';
  if ($msj==2)
  echo '<p align="center"> Problemas al Editar el Medico, favor Revisar';

?>
